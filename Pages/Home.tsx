import styles from './Home.module.scss';
const Home = () => {
    return (
        <section className={styles.quote}>
            <h1>CODITAS</h1>
            <p>Code is art , Code is us !</p>
        </section>
    )
}

export default Home;
