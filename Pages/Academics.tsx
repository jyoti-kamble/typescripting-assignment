const Academics = () => {
    return (
        <section>
            <table>
                
                    <tr>
                        <th>Sr No.</th>
                        <th>Qualification</th>
                        <th>Marks</th>
                    </tr>
                
                <tr>
                    <td>
                        1.
                    </td>
                    <td>
                        SSC
                    </td>
                    <td>
                        90%
                    </td>
                </tr>
                <tr>
                    <td>
                        2.
                    </td>
                    <td>
                        Diploma
                    </td>
                    <td>
                        80%
                    </td>
                </tr>
                <tr>
                    <td>
                        3.
                    </td>
                    <td>
                        Engineering
                    </td>
                    <td>
                        9.5CGPA
                    </td>
                </tr>
            </table>
        </section>
    )
}

export default Academics;
