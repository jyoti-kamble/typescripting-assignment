import { useState } from "react";
import Academics from "../../Pages/Academics/Academics";
import Certificates from "../../Pages/Certificates/Certificates";
import Home from "../../Pages/Home/Home";
import Profile from "../../Pages/Profile/Profile";
import Pill from "../Pill/Pill";
import styles from "./Main.module.scss";

const Main = () => {
  const [page, setPage] = useState<string>("");

  const navigateTo = (url: string) => setPage(url);

  return (
    <>
      <div className={styles.main}>
        <div className={styles.panel}>
          <Pill text="Home" onClick={() => navigateTo("")} />

          <Pill text="My Profile" onClick={() => navigateTo("profile")} />

          <Pill text="My Certificates" onClick={() => navigateTo("certificates")} />

          <Pill text="Academics" onClick={() => navigateTo("academics")} />

          
        </div>
        <main className={styles.info}>
          {page === "" && <Home />}

          {page === "profile" && <Profile />}
          {page === "certificates" && <Certificates />}
          {page === "academics" && <Academics />}
        </main>
      </div>
    </>
  );
};
export default Main;
