import styles from './Header.module.scss';

const Header = () => {
    return(
        <header className={styles.header}>
            <h1 className={styles.h1}>
                JYOTI ANIL KAMBLE
            </h1>
        </header>
    )
}
export default Header;