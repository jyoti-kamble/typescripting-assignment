import Header from '../Header/Header';
import Main from '../Main/Main';

const Layout = () => {
    return(
        <div>
            <Header/>
            <Main/>
        </div>
    )
}
export default Layout;